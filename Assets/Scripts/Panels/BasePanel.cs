﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WGameTest.Controllers;

public abstract class BasePanel : MonoBehaviour
{
    public abstract void Show();
    public abstract void Hide();

    public void Close()
    {
        ScreenController.Instance.CloseTop(null);
    }
    
    public void CloseAll()
    {
        ScreenController.Instance.CloseAll(null);
    }

    public virtual void CloseFromScreenController(Action callback)
    {
        if (callback != null)
        {
            callback.Invoke();
        }
    }
}
