﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WGameTest.Controllers;
using WGameTest.Models;
using WGameTest.Tools;

namespace WGameTest.Panels
{
    public class DetailsPanel : BasePanel
    {
        [SerializeField] private Text allLabels;
        [SerializeField] private RawImage poster = null;
        [SerializeField] private GameObject gridGO;
        
        private IGridController gridPresenter;
        private SearchItemModel model;
        private Coroutine imageLoader = null;
    
        private void Awake()
        {
            gridPresenter = gridGO.GetComponent<IGridController>();
            if (gridPresenter == null)
            {
                Debug.LogError("No grid on grid GameObject");
            }
        }

        public void Init(SearchItemModel model)
        {
            this.model = model;
            InitUI();
        }

        private void InitUI()
        {
            allLabels.text = string.Format("Title = {0} \n Year = {1} \n IMDB id = {2} \n Type = {3}"
                , model.Title, model.Year, model.imdbID, model.Type);
            
            if (imageLoader != null)
            {
                StopCoroutine(imageLoader);
            }
            imageLoader = ImageLoader.LoadImage(this, model.Poster, (texture)=> poster.texture = texture);
            LoadGrid();
        }

        private void LoadGrid()
        {
            gridPresenter.HideAll();
            int year = 2000;
            int.TryParse(model.Year, out year);
            
            DataProvider.LoadDataWithYear(this, gridPresenter.GridSize, model.searchKey, year - 1, (data) =>
            {
                gridPresenter.ShowItems(data);
            });
        }

        public override void Show()
        {
        }

        public override void Hide()
        {
        }
    }
}
