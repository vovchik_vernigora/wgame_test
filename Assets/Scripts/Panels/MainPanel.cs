﻿using System;
using UnityEngine;
using UnityEngine.UI;
using WGameTest.Controllers;
using WGameTest.Tools;
using Debug = UnityEngine.Debug;

namespace WGameTest.Panels
{
    public class MainPanel : BasePanel
    {
        [SerializeField] private InputField input;
        [SerializeField] private GameObject gridGO;
        
        private IGridController gridPresenter;

        private void Awake()
        {
            gridPresenter = gridGO.GetComponent<IGridController>();
            if (gridPresenter == null)
            {
                Debug.LogError("No grid on grid GameObject");
            }
        }

        public void OnInputUpdated(string inputStr)
        {
            if (input.text.Length < 3)
            {
                Debug.LogError("too small size of input text. use at least 3 symbols");
                return;
            }
            
            gridPresenter.HideAll();
            DataProvider.LoadData(this, gridPresenter.GridSize, input.text, (data) =>
            {
                gridPresenter.ShowItems(data);
            });
        }
        
        public override void Show()
        {
        }

        public override void Hide()
        {
        }
    }
}

