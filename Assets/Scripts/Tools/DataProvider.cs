﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using WGameTest.Models;

namespace WGameTest.Tools
{
    public static class DataProvider
    {
        public static void LoadData(MonoBehaviour mono, int maxDataAmountRequired, string serachData,
            Action<SearchResponseModel> callbackForLoadedChunk)
        {
            int requestNeeded = Mathf.CeilToInt(maxDataAmountRequired / 10f);

            for (int i = 1; i <= requestNeeded; i++)
            {
                mono.StartCoroutine(ELoadData(string.Format("http://www.omdbapi.com/?apikey=6367da97&s={0}&page={1}"
                    , UnityWebRequest.EscapeURL(serachData)
                    , i), serachData, i, callbackForLoadedChunk));
            }
        }

        public static void LoadDataWithYear(MonoBehaviour mono, int maxDataAmountRequired, string serachData, int year,
            Action<SearchResponseModel> callbackForLoadedChunk)
        {
            int requestNeeded = Mathf.CeilToInt(maxDataAmountRequired / 10f);

            for (int i = 1; i <= requestNeeded; i++)
            {
                mono.StartCoroutine(ELoadData(string.Format(
                    "http://www.omdbapi.com/?apikey=6367da97&s={0}&y={1}&page={2}"
                    , UnityWebRequest.EscapeURL(serachData)
                    , year
                    , i), serachData, i, callbackForLoadedChunk));
            }
        }

        private static IEnumerator ELoadData(string searchUrl, string searchString, int page,
            Action<SearchResponseModel> callbackForLoadedChunk)
        {
            yield return null;

            using (UnityWebRequest uwr = UnityWebRequest.Get(searchUrl))
            {

                Debug.LogFormat("loading url = {0}", uwr.url);

                yield return uwr.SendWebRequest();

                if (uwr.isNetworkError || uwr.isHttpError)
                {
                    Debug.Log(uwr.error);
                }
                else
                {
                    try
                    {
                        var result = JsonUtility.FromJson<SearchResponseModel>(uwr.downloadHandler.text);
                        for (int i = 0; i < result.Search.Count; i++)
                        {
                            result.Search[i].index = i + 10 * (page - 1);
                            result.Search[i].searchKey = searchString;
                        }

                        if (callbackForLoadedChunk != null)
                        {
                            callbackForLoadedChunk.Invoke(result);
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogErrorFormat("loaded data = {0}", uwr.downloadHandler.text);
                        Debug.LogError("can not parse the data" + e.Data);
                    }
                }
            }
        }
    }
}
