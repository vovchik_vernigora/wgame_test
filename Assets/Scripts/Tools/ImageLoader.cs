﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

namespace WGameTest.Tools
{
    public static class ImageLoader
    {
        public static Coroutine LoadImage(MonoBehaviour mono, string imageURL, Action<Texture> onLoaded)
        {
            return mono.StartCoroutine(EImageLoader(imageURL, onLoaded));
        }

        private static IEnumerator EImageLoader(string url, Action<Texture> onLoaded)
        {
            yield return null;

            var imageUri = GetFileURI(url); 
            if (HasImage(imageUri))
            {
                if (onLoaded != null)
                {
                    onLoaded(LoadImage(imageUri));
                }
            }

            using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(url))
            {
                yield return uwr.SendWebRequest();

                if (uwr.isNetworkError || uwr.isHttpError)
                {
                    Debug.LogFormat("error = {0}, url = {1}", uwr.error, url);
                    if (onLoaded != null)
                    {
                        onLoaded(null);
                    }
                }
                else
                {
                    var content = DownloadHandlerTexture.GetContent(uwr);
                    SaveImage(imageUri, content);
                    if (onLoaded != null)
                    {
                        onLoaded(content);
                    }
                    
                }
            }
        }

        private static string GetFileURI(string url)
        {
            return string.Format("{0}/{1}/{2}.png", Application.persistentDataPath, "ImageCache", url.GetHashCode());
        }

        private static Texture2D LoadImage(string fileURI)
        {
            //TODO: this should be coroutine
            if (!File.Exists(fileURI))
            {
                return null;
            }

            var fileData = File.ReadAllBytes(fileURI);
            var tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
            return tex;
        }

        private static bool HasImage(string fileURI)
        {
            try
            {
                CreateDirectory();
                return File.Exists(fileURI);
            }
            catch (Exception e)
            {
                Debug.LogErrorFormat("can not load file {0} {1}", fileURI, e);
            }

            return false;
        }

        private static void SaveImage(string fileURI,Texture2D textureToSave)
        {
            try
            {
                CreateDirectory();

                File.WriteAllBytes(fileURI, textureToSave.EncodeToPNG());
            }
            catch (Exception e)
            {
                Debug.LogErrorFormat("can not save file {0} {1}", fileURI, e);
            }
        }

        private static void CreateDirectory()
        {
            var dir = string.Format("{0}/{1}", Application.persistentDataPath, "ImageCache");
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
        }
    }
}
