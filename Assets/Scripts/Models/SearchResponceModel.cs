﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WGameTest.Models
{
    [Serializable]
    public class SearchResponseModel
    {
        public List<SearchItemModel> Search;
        public int totalResults;
        public bool Response;
        public string Error;
    }
}
