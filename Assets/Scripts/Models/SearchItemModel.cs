﻿using System;

namespace WGameTest.Models
{
    [Serializable]
    public class SearchItemModel
    {
        public string Title;
        public string Year;
        public string imdbID;
        public string Type;
        public string Poster;
        public int index;
        public string searchKey;
    }
}


