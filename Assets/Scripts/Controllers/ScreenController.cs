﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WGameTest.Controllers
{
    [RequireComponent(typeof(Canvas))]
    public class ScreenController : MonoBehaviour
    {
        //User service locator here, but for now we can use Singleton

        public static ScreenController Instance;

        private static Stack<BasePanel> panels = new Stack<BasePanel>();
        
        void Awake()
        {
            Instance = this;
            // NOTE!!! I do not added Main panel here, as this require more time,
            // and i'm already tied, so scare that can make more mistakes
        }

        public T ShowPanel<T>() where T : BasePanel
        {
            if (panels.Count > 0)
            {
                panels.Peek().Hide();
            }

            var panelResource = Resources.Load<T>(string.Format("Panels/{0}", typeof(T).Name));
            if (panelResource == null)
            {
                throw new Exception(string.Format("No panel type found {0}", typeof(T).Name));
            }

            var panel = Instantiate<T>(panelResource, transform);
            panels.Push(panel);
            panel.Show();
            return panel;
        }

        public void CloseTop(Action callback)
        {
            if (panels.Count == 0)
            {
                if (callback != null)
                {
                    callback.Invoke();
                }
                return;
            }

            var panel = panels.Pop();
            panel.CloseFromScreenController(callback);
            Destroy(panel.gameObject);
        }
        
        public void CloseAll(Action callback)
        {
            if (panels.Count == 0)
            {
                if (callback != null)
                {
                    callback.Invoke();
                }
                return;
            }

            CloseTop(() => CloseAll(callback));
        }
    }
}
