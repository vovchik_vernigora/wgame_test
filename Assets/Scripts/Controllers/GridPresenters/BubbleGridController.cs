﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WGameTest.Models;

namespace WGameTest.Controllers
{
    public class BubbleGridController : MonoBehaviour, IGridController
    {
        [SerializeField] private SerachItemController SerachItemControllerPrefab;
        [SerializeField] private float gapVertical = 0.02f;
        [SerializeField] private float gapHorisontal = 0.02f;
        [SerializeField] private int widthElementsCount = 4;
        [SerializeField] private int heightElementsCount = 3;
        
        private RectTransform grid;

        private List<SerachItemController> controllers = new List<SerachItemController>();
        
        private Queue<SearchItemModel> itemsToShow = new Queue<SearchItemModel>();
        
        /// current index to show data. If is -1 - means that all elements showed
        private int curentShowedIndex = -1;
        
        public int GridSize {
            get { return heightElementsCount * widthElementsCount; }
        }

        private void Awake()
        {
            grid = gameObject.transform as RectTransform;

            var rect = grid.rect;
            var elementWidth = (rect.width - rect.width * gapHorisontal * (widthElementsCount + 1)) / widthElementsCount;
            var elementHeight = (rect.height - rect.height * gapVertical * (heightElementsCount + 1)) / heightElementsCount;
            var gapSizeVertical = rect.height * gapVertical;
            var gapSizeHorizontal = rect.width * gapHorisontal;
            for (int i = 0; i < heightElementsCount; i++)
            {
                for (int j = 0; j < widthElementsCount; j++)
                {
                    var controller = Instantiate(SerachItemControllerPrefab, grid);
                    
                    controller.Init(controllers.Count, 
                        elementWidth, 
                        elementHeight, 
                        gapSizeHorizontal * (j + 1) + elementWidth * j, 
                        rect.height - gapSizeVertical * (i + 1) - elementHeight * i);
                    
                    controllers.Add(controller);
                }
            }
        }

        public void HideAll()
        {
            foreach (var c in controllers)
            {
                c.Hide(null);
            }
            curentShowedIndex = -1;
            itemsToShow.Clear();
        }

        public void ShowItems(List<SearchItemModel> elToShow)
        {
            foreach (var el in elToShow)
            {
                if (el.index >= controllers.Count)
                {
                    break;
                }

                itemsToShow.Enqueue(el);    
            }

            if (curentShowedIndex == -1)
            {
                ShowInQueue();
            }
        }

        public void ShowItems(SearchResponseModel model)
        {
            ShowItems(model.Search);
        }

        
        private void ShowInQueue()
        {
            if (itemsToShow.Count == 0)
            {
                curentShowedIndex = -1;
                return;
            }
            
            var elToShow = itemsToShow.Dequeue();
            curentShowedIndex++;
            
            controllers[curentShowedIndex].Hide(() =>
            {
                if (curentShowedIndex == -1)
                {
                    return;
                }

                controllers[curentShowedIndex].Show(ShowInQueue);
                controllers[curentShowedIndex].SetSearchItemData(elToShow);
            });
        }
    }
}
