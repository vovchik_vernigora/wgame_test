﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WGameTest.Models;

namespace WGameTest.Controllers
{
    public interface IGridController
    {
        int GridSize { get; }
        void HideAll();
        void ShowItems(List<SearchItemModel> elToShow);
        void ShowItems(SearchResponseModel model);
    }
}
