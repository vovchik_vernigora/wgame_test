﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using WGameTest.Models;
using WGameTest.Panels;
using WGameTest.Tools;

namespace WGameTest.Controllers
{
    [RequireComponent(typeof(Animator))]
    public class SerachItemController : MonoBehaviour
    {
        private const string SHOWED_KEY = "Showed";
        
        [SerializeField] private RawImage poster = null;
        [SerializeField] private Text title = null;

        private SearchItemModel modelToSet = null;

        private Coroutine imageLoader = null;
        private RectTransform tr = null;
        private Animator animator;

        private int index;
        private Action showedCallback;
        private Action hiddenCallback;
        
        private void Awake()
        {
            tr = gameObject.transform as RectTransform;
            animator = gameObject.GetComponent<Animator>();
        }

        public void Init(int index, float width, float height, float x, float y)
        {
            this.index = index;
            tr.anchoredPosition = new Vector2(x, y);
            tr.sizeDelta = new Vector2(width, height);
        }

        public void Show(Action showedCallback)
        {
            if (animator.GetBool(SHOWED_KEY))
            {
                if (showedCallback != null)
                {
                    showedCallback.Invoke();
                };
                return;
            }

            animator.SetBool(SHOWED_KEY, true);
            this.showedCallback = showedCallback;
        }

        public void CardShowed()
        {
            if (showedCallback != null)
            {
                showedCallback.Invoke();
            }
        }

        public void Hide(Action hiddenCallback)
        {
            if (!animator.GetBool(SHOWED_KEY))
            {
                if (hiddenCallback != null)
                {
                    hiddenCallback.Invoke();
                }
                return;
            }
            
            animator.SetBool(SHOWED_KEY, false);
            this.hiddenCallback = hiddenCallback;
        }

        public void CardHidden()
        {
            if (hiddenCallback != null)
            {
                hiddenCallback.Invoke();
            }
        }
        
        public void SetSearchItemData(SearchItemModel modelToSet)
        {
            this.modelToSet = modelToSet;
            UpdateView();
        }

        public void OnClick()
        {
            var panel = ScreenController.Instance.ShowPanel<DetailsPanel>();
            panel.Init(modelToSet);
        }

        private void UpdateView()
        {
            title.text = modelToSet.Title;
        
            if (imageLoader != null)
            {
                StopCoroutine(imageLoader);
            }
            imageLoader = ImageLoader.LoadImage(this, modelToSet.Poster, (texture)=> poster.texture = texture);
        }
    }
}
